.. radar documentation master file, created by
   sphinx-quickstart on Fri Feb 22 16:39:23 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to radar's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


************
View функции
************
.. automodule:: first.views
   :members:

******
Модели
******
.. automodule:: first.models
   :members:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
