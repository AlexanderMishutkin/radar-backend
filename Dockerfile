FROM python:3.6

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-client \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/

RUN pip install --no-cache-dir -r requirements.txt


COPY . /usr/src/app

EXPOSE 8000

RUN ["python", "manage.py", "makemigrations"]
RUN ["python", "manage.py", "migrate", "--run-syncdb"]

RUN cd docs\
    && make html

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]