import json
import django.http
from django.core import serializers
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from first.models import Advert, Seller, Developer
import hashlib

# Create your views here.


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    """
    API, позволяющий авторизовать пользователей,
    дает токен для аутентифкации по логину и паролю.
    В дату кладем имя и пароль в формате JSON.

    Админка: {"username":"alex","password":"qwerty11111111"}

    :param request (JSON):
    :return: auth token:
    """
    username = request.data.get("username")
    password = request.data.get("password")
    user = authenticate(username=username, password=password)
    if user:
        token, _ = Token.objects.get_or_create(user=user)
        type_1 = ''
        if Seller.objects.filter(user=user.id).count() == 0:
            type_1 = 'developer'
        else:
            type_1 = 'advertiser'
        return Response({'token': token.key, 'type': type_1})
    return Response({'error': 'no such user'})


@csrf_exempt
@api_view(["POST", "GET"])
def get_username(request):
    """
    Тестовый API, позволяющий посмотреть имя пользователя,
    у которого есть токен.
    Токен должен лежать в заголовке HTTP запроса,
    а именно в <<Authorization : token 8de94977b46e402e4fb2d4921bd18353b2fff20e>>
    для отправки таких запросов руками используйте программку postman,
    но для debug'а можете просто выбирать случайного пользователя из модели и добавить декоратор
    allow any как в api/login
    Админка: {"username":"alex","password":"qwerty11111111"}

    :param request (JSON):
    :return: username:
    """
    return Response({'username': request.user.username})


@csrf_exempt
@api_view(["POST", "GET"])
def create_ad(request):
    """
    Создает новую модель рекламы (Advert), по запросу:
    http запрос, dataType : formdata
    ['ad'] : { (Да, да, а дальше все в формате JSON(не осуждайте за костыль ) ))
        coords[0..1]
        imageUrl - Ссылка на объект
        name - Название рекламы
        description - Описание рекламы
        payment - Способ оплаты
        (по умолчанию ноль)amount_of_views = 0 - Количество просмотров рекламы
    """
    user = User.objects.filter(username=request.user)[0]
    seller = Seller.objects.filter(user=user)[0]
    advert_1 = json.loads(request.POST.get('ad', None))
    if Advert.objects.filter(special_id=advert_1['id']).count() > 0:
        advert_2 = Advert.objects.filter(special_id=advert_1['id'])[0]
        advert_2.seller = seller
        advert_2.latitude = advert_1['coords'][0]
        advert_2.longitude = advert_1['coords'][1]
        advert_2.url = advert_1['URL']
        advert_2.name = advert_1['name']
        advert_2.description = advert_1['description']
        advert_2.payment = advert_1['payment']
        advert_2.save()
    else:
        Advert.objects.create(seller=seller, special_id=Advert.objects.count(),
                              latitude=advert_1['coords'][0], longitude=advert_1['coords'][1],
                              url=advert_1['URL'], name=advert_1['name'],
                              description=advert_1['description'], payment=advert_1['payment'])
    return django.http.HttpResponse('ok')


@csrf_exempt
@api_view(["POST", "GET"])
@permission_classes((AllowAny,))
def ad_watched(request, token, name):
    """
    Функция получает имя просмотренной пользователем рекламы и
    увеличивает количество просмотров рекламы с дааным именем
    """
    advert = Advert.objects.get(name=name)
    developer = Developer.objects.get(developer_token=token)
    print('token = ', token, ', name = ', name)
    developer.amount_of_views += 1
    advert.amount_of_views += 1
    developer.save()
    advert.save()
    return django.http.HttpResponse('ad_name = {}'.format(name))


@csrf_exempt
@api_view(["POST", "GET"])
def get_ads(request):
    """
    Возвращает все имеющиеся модели ad авторизованого пользователя.
    Формат ответа - JSON
    """
    user = User.objects.filter(username=request.user)[0]
    seller = Seller.objects.filter(user=user)[0]
    qs_json = serializers.serialize('json', Advert.objects.filter(seller=seller))
    return Response(qs_json)


def dist(x_1, y_1, x_2, y_2):
    '''
    Локальная функция для нахождения расстояния между 2 точками на плоскости.
    '''
    return ((x_1 - x_2) ** 2 + (y_1 - y_2) ** 2) ** 0.5


DEBUG_STR = ''


@csrf_exempt
@api_view(["POST", "GET"])
@permission_classes((AllowAny,))
def get_nearest_ad(request, latitude, longitude):
    """
    Возвращает ближайшую к пользователю рекламу по получаемым координатам.
    Формат ответа - JSON
    """
    test_ads = list(Advert.objects.filter(id=1))
    result = ['error503']
    if not test_ads:
        result = ['error503']
    else:
        test_ad = test_ads[0]
        min_dist_ad_id = test_ad.id
        min_dist = dist(float(latitude), float(longitude),
                        float(test_ad.latitude), float(test_ad.longitude))
        for advert in Advert.objects.all():
            ad_dist = dist(float(latitude), float(longitude),
                           float(advert.latitude), float(advert.longitude))
            if ad_dist < min_dist:
                min_dist = ad_dist
                min_dist_ad_id = advert.id
        advert = Advert.objects.get(id=min_dist_ad_id)
        result = {
            'special_id': advert.special_id,
            'latitude': advert.latitude,
            'longitude': advert.longitude,
            'url': 'http://nevbros.ru:3333' + advert.url,
            'name': advert.name,
            'description': advert.description,
            'payment': advert.payment,
            'amount_of_views': advert.amount_of_views
        }
    return Response(result)


@csrf_exempt
@api_view(["POST", "GET"])
@permission_classes((AllowAny,))
def registration(request):
    """
    Работает как api/login

    :param request:
    :return: auth token
    """
    username = request.data.get("username", None)
    password = request.data.get("password", None)
    email = request.data.get("email", None)
    if username and password and email:
        if not User.objects.all().filter(username=username).exists():
            user = User.objects.create_user(username, email, password)
            if request.data.get("type", None) == 'developer':
                developer = Developer.objects.create(user=user)
                if developer.developer_token == "":
                    developer.developer_token = hashlib.md5(str(user.id).encode()).hexdigest()
                developer.save()
            if request.data.get("type", None) == 'advertiser':
                Seller.objects.create(user=user)
        else:
            return Response({'error': 'username already exist'})
    token, _ = Token.objects.get_or_create(user=user)
    return Response({'token': token.key})


@csrf_exempt
@api_view(["POST", "GET"])
def amount_of_views(request):
    """
    Возвращает количество просмотров рекламы на
        всех приложениях авторизованого разработчика.

    :param http header authorization вида << authorization:token 56jd743hdfuhks76 >>:
    :return JSON, где кол-во просмотров находится под ключем <<amount_of_views>>:
    """
    user = User.objects.filter(username=request.user)[0]
    developer = Developer.objects.filter(user=user)[0]
    return Response({'amount_of_views': developer.amount_of_views, 'developer_token': developer.developer_token})


@csrf_exempt
@api_view(["POST"])
def edit_user(request):
    """
    Получает имя и/или пароль и/или почту в виде formData и
        заменяет соответствующие поля в модели user
    :param request:
    :return: error if exists
    """
    user = request.user
    username = request.data.get("username", None)
    password = request.data.get("password", None)
    email = request.data.get("email", None)
    if username:
        if not User.objects.all().filter(username=username).exists():
            user.username = username
        else:
            return Response({'error': 'username already exist'})
    if password:
        user.password = password
    if email:
        user.email = email
    user.save()
    return Response({'error': ''})
