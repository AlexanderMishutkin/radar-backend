from django.db import models
from django.contrib.auth.models import User


# Продавец
class Seller(models.Model):
    """
    Класс продавец, в котором содержатся поля:
        user_id - связывает продавца с зарегистрированным пользователем
    """
    user = models.ForeignKey(
        User,
        models.SET_NULL,
        blank=True,
        null=True,
    )  # ID продавца


# Реклама
class Advert(models.Model):
    """
    Класс реклама, в которой содержатся поля:
        special_id - уникальное id рекламы
        latitude - широта,
        longitude - долгота,
        url - ссылка на расположение объекта на сервере,
        seller - продавец, которому при надлежит реклама,
        name - название рекламы,
        description - описание рекламы,
        payment - способ оплаты,
        amount_of_views - количество просмотров рекламы
    """

    special_id = models.IntegerField(unique=True)  # Специальное id для настроек
    latitude = models.FloatField(null=False)  # Широта
    longitude = models.FloatField(null=False)  # Долгота
    url = models.CharField(null=False, max_length=200)  # Ссылка на объект
    seller = models.ForeignKey(Seller, on_delete=models.CASCADE, null=True)  # Имя продавца
    name = models.CharField(null=False, max_length=200)  # Название рекламы
    description = models.CharField(null=False, max_length=200)  # Описание рекламы
    payment = models.CharField(null=False, max_length=200)  # Способ оплаты
    amount_of_views = models.IntegerField(null=False, default=0)  # Количество просмотров рекламы


# Разработчик
class Developer(models.Model):
    """
    Класс разработчик, в котором содержатся поля:
        user_id - связывает разработчика с зарегистрированным пользователем,
        developer_token - специальный токен разработчика,
        amount_of_views - количество просмотров всей рекламы, размещаемой у разработчика
    """
    user = models.ForeignKey(
        User,
        models.SET_NULL,
        blank=True,
        null=True,
    )  # ID разработчика
    developer_token = models.CharField(null=False, max_length=200)  # Токен разработчика
    amount_of_views = models.IntegerField(null=False,
                                          default=0)  # Количество просмотров
    # всей рекламы размещаемой разработчиком
