from django.test import TestCase, Client
from .models import Advert, Seller, Developer, User
from .views import dist


class DatabaseTest(TestCase):

    def test_advert_model(self):
        '''
        Тест проверяющий корректное создание 'рекламы' в базе данных
        '''
        advert = Advert(latitude=55.123,
                        longitude=86.2345,
                        url='C:/static/images/test.png',
                        name='test_advert',
                        description='advert just for test',
                        payment='test_payment',
                        special_id=Advert.objects.count())
        advert.save()
        test = Advert.objects.get(name='test_advert')
        self.assertEqual(test.latitude, 55.123)
        self.assertEqual(test.longitude, 86.2345)
        self.assertEqual(test.url, 'C:/static/images/test.png')
        self.assertEqual(test.description, 'advert just for test')
        self.assertEqual(test.payment, 'test_payment')

    def test_seller_model(self):
        '''
        Тест проверяющий корректное создание 'продавца' в базе данных
        '''
        user_1 = User(id=42,
                      password='qwerty',
                      username='testuser')
        user_1.save()
        user_2 = Seller(user_id=42,
                        id=3)
        user_2.save()
        test = Seller.objects.get(id=3)
        self.assertEqual(test.id, 3)
        self.assertEqual(test.user_id, 42)

    def test_developer_model(self):
        '''
        Тест проверяющий корректное создание 'разработчика' в базе данных
        '''
        user_1 = User(id=21,
                      password='qwerty',
                      username='testuser')
        user_1.save()
        user_2 = Developer(id=5,
                           user_id=21,
                           developer_token='no user no token')
        user_2.save()
        test = Developer.objects.get(id=5)
        self.assertEqual(test.user_id, 21)
        self.assertEqual(test.developer_token, 'no user no token')

    def test_reg_to_users_with_equal_username(self):
        '''
        См название тетса
        '''
        # '''
        # См название тетса
        # '''
        client = Client()
        client.post("/api/registration",
                    data={"username": "testuser", "password": "testuser", "email": "test1"})
        request = client.post("/api/registration",
                              data={"username": "testuser",
                                    "password": "testuser", "email": "test"})
        self.assertEqual(str(request.data), "{'error': 'username already exist'}")

    def test_reg_to_users_with_equal_email(self):
        '''
        См название тетса
        '''

    def test_change_to_an_exsisting_username(self):
        client = Client()
        client.post("/api/registration",
                    data={"username": "testuser1", "password": "testuser", "email": "test1"})
        resp = client.post("/api/registration",
                           data={"username": "testuser2", "password": "testuser", "email": "test1"})
        header = {'HTTP_AUTHORIZATION': 'Token ' + resp.data['token']}
        request = client.post("/api/edit",
                              data={"username": "testuser1",
                                    "password": "testuser", "email": "test1"},
                              **header)
        self.assertEqual(str(request.data), "{'error': 'username already exist'}")

    def test_no_such_user_in_login(self):
        client = Client()
        client.post("/api/registration",
                    data={"username": "testuser1", "password": "testuser", "email": "test1"})
        request = client.post("/api/login", data={"username": "testuser2", "password": "testuser"})
        self.assertEqual(str(request.data), "{'error': 'no such user'}")

    def test_wrong_pass(self):
        client = Client()
        client.post("/api/registration",
                    data={"username": "testuser1", "password": "testuser", "email": "test1"})
        request = client.post("/api/login", data={"username": "testuser1", "password": "testuser1"})
        self.assertEqual(str(request.data), "{'error': 'no such user'}")

    def test_correct_login(self):
        client = Client()
        client.post("/api/registration",
                    data={"username": "testuser1", "password": "testuser", "email": "test1"})
        request = client.post("/api/login", data={"username": "testuser1", "password": "testuser"})
        self.assertTrue(str(request.data) != "{'error': 'no such user'}")

    def test_get_username(self):
        client = Client()
        resp = client.post("/api/registration",
                           data={"username": "testuser", "password": "testuser", "email": "test1"})
        header = {'HTTP_AUTHORIZATION': 'Token ' + resp.data['token']}
        request = client.post("/api/get_username",
                              **header)
        self.assertEqual(str(request.data), "{'username': 'testuser'}")

    def test_correct_edit(self):
        client = Client()
        client.post("/api/registration",
                    data={"username": "testuser1", "password": "testuser", "email": "test1"})
        resp = client.post("/api/registration",
                           data={"username": "testuser2", "password": "testuser", "email": "test1"})
        header = {'HTTP_AUTHORIZATION': 'Token ' + resp.data['token']}
        request = client.post("/api/edit",
                              data={"username": "testuser3",
                                    "password": "testuser", "email": "test1"},
                              **header)
        self.assertTrue(str(request.data) != "{'error': 'username already exist'}")

    def test_reg_types(self):
        client = Client()
        client.post("/api/registration",
                    data={"username": "testuser1",
                          "password": "testuser", "email": "test1", "type": "developer"})
        request = client.post("/api/registration",
                              data={"username": "testuser2",
                                    "password": "testuser", "email": "test2",
                                    "type": "advertiser"})
        self.assertTrue(str(request.data) != {'error': 'username already exist'})

    def test_amount_of_views(self):
        client = Client()
        resp = client.post("/api/registration",
                           data={"username": "testuser1",
                                 "password": "testuser",
                                 "email": "test1",
                                 "type": "developer"})

        header = {'HTTP_AUTHORIZATION': 'Token ' + resp.data['token']}
        request = client.post("/api/amount_of_views", **header)
        self.assertEqual((str(request.data)).find("'amount_of_views': 0") >= 0, True)

    def test_get_nearest_ad(self):
        '''
        Тест для проверки функции get_nearest_ad,
        а именно для проверки правильного нахождения ближайшей к пользователю рекламы
        :return:
        '''
        client = Client()
        user = User(id=21, password='qwerty', username='testuser')
        user.save()
        ad1 = Advert(special_id=2, latitude=57.9204,
                     longitude=36.8306, url="C:/images/images/test2.png",
                     name="testad2", description="ad for test #2", payment="test")
        ad1.save()
        ad2 = Advert(special_id=1, latitude=55.6820,
                     longitude=37.5205, url="C:/images/images/test1.png",
                     name="testad1", description="ad for test #1", payment="test")
        ad2.save()
        request = client.get("/api/get_nearest_ad/56.2056+37.1025")
        self.assertEqual(str(request.data["special_id"]), "1")
        self.assertEqual(str(request.data["name"]), "testad1")

    def test_get_nearest_ad_no_ads(self):
        '''
        Тест для проверки функции get_nearest_ad,
        а именно для проверки правильного нахождения ближайшей к пользователю рекламы
        :return:
        '''
        client = Client()
        request = client.get("/api/get_nearest_ad/56.2056+37.1025")
        self.assertEqual(str(request.data), "['error503']")

    def test_login_advertiser(self):
        client = Client()
        client.post("/api/registration",
                    data={"username": "testuser1", "password": "testuser",
                          "email": "test1", "type": "advertiser"})
        request = client.post("/api/login", data={"username": "testuser1", "password": "testuser"})
        self.assertTrue(str(request.data) != "{'error': 'no such user'}")

    def test_dist_func(self):
        '''
        Тест для проверки функции dist, находящей расстояние между двумя точками
        :return:
        '''
        x_1 = 55.234
        x_2 = 54.9506
        y_1 = 36.0284
        y_2 = 37.9938
        self.assertAlmostEqual(dist(x_1, y_1, x_2, y_2), 1.985727252167326)

    def test_ad_watched_func(self):
        '''
        Тест для проверки функции, увеличивающей количество просмотров рекламы с данным именем
        '''
        client = Client()
        resp = client.post("/api/registration",
                           data={"username": "testuser",
                                 "password": "qwerty",
                                 "email": "test1",
                                 "type": "developer"})
        advert_1 = Advert(latitude=55.123,
                          longitude=86.2345,
                          url='C:/static/images/test.png',
                          name='test_advert',
                          description='advert just for test',
                          payment='test_payment',
                          special_id=Advert.objects.count())
        advert_1.save()
        resp = client.post("/api/login",
                           data={"username": "testuser",
                                 "password": "qwerty"})
        print(resp.data)
        header = {'HTTP_AUTHORIZATION': 'Token ' + resp.data['token']}
        user_1 = User.objects.get(username='testuser')
        test = Developer.objects.get(user_id=user_1.id)
        test.developer_token = '123456789'
        prev_dev = test.amount_of_views
        test.save()
        prev_ad = advert_1.amount_of_views
        print(test.developer_token)
        print(prev_dev, ' iksdbiusbd ', prev_ad)
        client.post("/api/ad_watched/123456789+test_advert", **header)
        test1 = Developer.objects.get(developer_token='123456789')
        test2 = Advert.objects.get(name='test_advert')
        self.assertEqual(test1.amount_of_views - prev_dev, 1)
        self.assertEqual(test2.amount_of_views - prev_ad, 1)

    def test_create_add(self):
        client = Client()
        resp = client.post("/api/registration",
                           data={"username": "testuser2", "password": "testuser", "email": "test1",
                                 "type": "advertiser"})
        headers = {'HTTP_AUTHORIZATION': 'Token ' + resp.data['token']}
        client.post("/api/add_ad", data={
            "ad": "{\"coords\": [0,0],\"name\": \"test\",\"description\": \"test\","
                  "\"payment\": \"test\",\"URL\": \"test\", \"id\": \"57\"}",
            "qw": "qw"}, **headers)
        client.post("/api/add_ad", data={
            "ad": "{\"coords\": [0,1],\"name\": \"test\",\"description\": \"test\","
                  "\"payment\": \"test\",\"URL\": \"test\", \"id\": \"0\"}",
            "qw": "qw"}, **headers)
        self.assertEqual(0, 0)

    def test_get_ads(self):
        client = Client()
        resp = client.post("/api/registration",
                           data={"username": "testuser2", "password": "testuser", "email": "test1",
                                 "type": "advertiser"})
        headers = {'HTTP_AUTHORIZATION': 'Token ' + resp.data['token']}
        client.post("/api/add_ad", data={
            "ad": "{\"coords\": [0,0],\"name\": \"test\",\"description\": \"test\","
                  "\"payment\": \"test\",\"URL\": \"test\", \"id\": \"57\"}",
            "qw": "qw"}, **headers)
        client.post("/api/get_ads", **headers)
        self.assertEqual(0, 0)
