from django.shortcuts import render, HttpResponse

from .forms import ImageUploadForm
from .models import Image
import django.http
from django.http import JsonResponse
from django.core import serializers
from django.http import HttpResponse
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from first.models import Advert, Seller, Developer
import json
import datetime


@csrf_exempt
@permission_classes((AllowAny,))
def upload_image(request):
    """
    This function can save your image
    :param request:
    :return :
    """
    context = {}
    form = ImageUploadForm()
    if request.method == 'POST':
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            context['ok'] = 'Your image was uploaded, you can upload another image again'
        else:
            context['ok'] = 'Woops, something went wrong, try again.'
    else:
        context['ok'] = 'Upload your image now (if you want)'
    context['form'] = form
    list = []  # myfile is the key of a multi value dictionary, values are the uploaded files
    for f in request.FILES.getlist('img'):  # myfile is the name of your html file button
        filename = f.name
        list.append(filename)
    if len(list) > 0:
        return HttpResponse(list[0])
    else:
        return render(request, 'file_upload.html' ,context)
