#!/bin/bash

rating=$(pylint -j 0 --load-plugins=pylint_django --disable=C0111 first/*.py | tail -2 | grep -oE ' [0-9]{1,2}' | grep -oE '[0-9]{1,2}')
hight=7
low=5
if [ "$rating" -gt "$hight" ]
then
pylint -j 0 --load-plugins=pylint_django --disable=C0111 first/*.py
echo -e  "\nGreat! Good work!"
exit 0
fi
if [ "$rating" -gt "$low" ]
then
pylint -j 0 --load-plugins=pylint_django --disable=C0111 first/*.py
echo -e  "\nNot bad, but need refactoring..."
exit 0
fi
pylint -j 0 --load-plugins=pylint_django --disable=C0111 first/*.py
echo -e  "\nYou didn't pass >= 6.0 limit\n"
echo -e  "\nThis code is awfull! Refactor it!"
exit 1
