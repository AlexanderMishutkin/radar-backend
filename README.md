<dl>
<a href="https://gitlab.informatics.ru/Mishutkin/radar-backend/commits/dev"><img alt="pipeline status" src="https://gitlab.informatics.ru/Mishutkin/radar-backend/badges/dev/pipeline.svg" /></a>
<a href="https://gitlab.informatics.ru/Mishutkin/radar-backend/commits/dev"><img alt="coverage report" src="https://gitlab.informatics.ru/Mishutkin/radar-backend/badges/dev/coverage.svg" /></a>
<br/><i>Название, кстати, ссылочка на работающий сайт:</i>
<h1><a href="http://nevbros.ru:3333"> rAdAr </a></h1>
<h2> а вот ссылочка на <a href="http://nevbros.ru:3333/documentation/">докуметацию</a></h2>
<h3> Новости: </h3>
<ul>
<li>Proxy наконец-то нормально заработал</li>
<li>Появилось задание на ближайшую неделю (см. далее)</li>
</ul>
<hr/>
<h3>Задание на неделю (обновлено 15.05.2019, ах как время быстро идет):</h3>
<ul>
<li>
Олег:
<ol>
Получение JSON'а на Jave
</ol>
</li>
<li>
Максим:
<ol>
Подчистить за собой pep8 и pylint(чуть-чуть)
</ol>
<ol>
Тест для get_nearest_ad
</ol>
</li>
<li>
Артем:
<ol>
View.py функция для просмотра токена разработчика
</ol>
</li>
<li>
Иван:
<ol>
Сделать django test'ы (ещё чуть-чуть) для существующих страничек API (логин и регистрация, добавление рекламы, получение списка реклам)
</ol>
</li>
<li>
Я:
<ol>
Изменение данных аккаунта.
</ol>
</li>
</ul>
<hr/>

<h2>!!! делаем все эти задания в новых ветках !!!, <b>Но начинаем вливаться в dev</b></h2>


<h4><i>(Возможны некоторые проблемы из-за огромного gitignore'а, сообщайте.)</i></h4>

<h3> Не забываем: </h3>
<ul>
<li>как нибудь про'merge'ится с dev чтобы ваша ветка поддерживала CI/CD</li>
<li>```pip install -r requirements.txt```</li>
<li>```python manage.py makemigrations```</li>
<li>```python manage.py migrate```</li>
<li>```make html``` (или ```docs/make.bat html``` на винде)</li>
</ul>
<img src="https://i.ibb.co/wS8hhnL/vue.png" alt="rAddAr"/>
</dl>
